package com.example.shopping;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.shopping.ShoppingContract.ShopEntry;


public class ShopDBHelper extends SQLiteOpenHelper {
	
	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "Shop.db";
	
	private static final String SQL_CREATE_ENTRIES = 
			"CREATE TABLE " + ShopEntry.TABLE_NAME 
			+ " (" + ShopEntry._ID + " INTEGER PRIMARY KEY, "
			+ ShopEntry.COLUMN_NAME_TITLE + " TEXT, "
			+ ShopEntry.COLUMN_NAME_QUANTITY + " INTEGER)";
	
	private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + ShopEntry.TABLE_NAME;
	
	
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_ENTRIES);
	}
	
	public ShopDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(SQL_DELETE_ENTRIES);
		onCreate(db);
	}
	

}
