package com.example.shopping;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.shopping.ShoppingContract.ShopEntry;

public class View_items extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_items);
		ShopDBHelper mDbHelper = new ShopDBHelper(this);
		SQLiteDatabase db = mDbHelper.getReadableDatabase();
		
		
		String[] projection = {ShopEntry._ID, ShopEntry.COLUMN_NAME_TITLE, ShopEntry.COLUMN_NAME_QUANTITY};
		
		String sortOrder = ShopEntry.COLUMN_NAME_TITLE + " DESC";
		
		Cursor c = db.query(ShopEntry.TABLE_NAME, projection, null, null,null,null,sortOrder);
		ListView list = (ListView) findViewById(R.id.list);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);

		if(c.moveToFirst()){
			do{
				String title = c.getString(c.getColumnIndex(ShopEntry.COLUMN_NAME_TITLE));
				String quantity = c.getString(c.getColumnIndex(ShopEntry.COLUMN_NAME_QUANTITY));
				adapter.add(title);
				adapter.add(quantity);
			
			}while(c.moveToNext());
			list.setAdapter(adapter);
		}

		db.close();
		
	}

}


