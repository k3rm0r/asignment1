package com.example.shopping;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.example.shopping.ShoppingContract.ShopEntry;

public class NewItemActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_item);

	}


	public void saveList(View view) {
		/*
		SQLiteDatabase db = mDbHelper.getWriteableDatabase();

		ContentValues values = new ContentValues();
		values.put(ShopEntry.COLUMN_NAME_ITEM, item);
		values.put(ShopEntry.COLUMN_NAME_QUANTITY, quantity);

		long newRowID;
		newRowId = db.insert( ShopEntry.TABLE_NAME, ShopEntry,COLUMN_NAME_NULLABLE, values);
		
		*/
		ShopDBHelper mDBHelper = new ShopDBHelper(this);
		SQLiteDatabase db = mDBHelper.getWritableDatabase();
		
		EditText title = (EditText) findViewById(R.id.shop_item);
		EditText quantity = (EditText) findViewById(R.id.shop_quantity);
		
		String titlestring = title.getText().toString();
		String quantitystring = quantity.getText().toString();
		
		ContentValues values = new ContentValues();
		
		values.put(ShopEntry.COLUMN_NAME_TITLE, titlestring);
		values.put(ShopEntry.COLUMN_NAME_QUANTITY, quantitystring);
		
		db.insert(ShopEntry.TABLE_NAME, null, values);
		
		db.close();
		finish();
	}
	

	
}


