package com.example.shopping;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

	public class MainActivity extends Activity {

		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_main);
		}



		@Override
		protected void onStart() {
			super.onStart();
		}
		
		@Override
		protected void onResume() {
			super.onResume();
		}

		@Override
		protected void onPause() {
			super.onPause();
		}
		
		@Override
		protected void onStop() {
			super.onStop();
		}
		
		@Override
		protected void onRestart() {
			super.onRestart();
		}
		
		@Override
		protected void onDestroy() {
			super.onDestroy();
		}


	public void addItem (View view) {
		Intent intent = new Intent(this, New_item.class);
		startActivity(intent);

	}

	/*public void removeItem (View view) {
		Intent intent = new Intent(this, Remove_item.class);
		startActivity(intent);

	}
*/
	public void viewList (View view) {
		Intent intent = new Intent(this, View_items.class);
		startActivityForResult(intent, 1);
	}
	}