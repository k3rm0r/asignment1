package com.example.shopping;

import android.provider.BaseColumns;

public class ShoppingContract {
	public ShoppingContract(){
		
	}
	
	public static abstract class ShopEntry implements BaseColumns {
		
		public static final String TABLE_NAME = "entry";
		public static final String COLUMN_NAME_TITLE = "title";
		public static final String COLUMN_NAME_QUANTITY = "quantity";
		
	}

}
